#! /bin/sh

xhost +local:docker

WD=$(pwd)

docker run \
    -e DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --env QT_X11_NO_MITSHM=1 \
    --mount type=bind,source=$WD/data,target=/root \
    -d \
    -p 10000:22 \
    fut

# Flags:
# https://stackoverflow.com/questions/24095968/docker-for-gui-based-environments
# https://github.com/osrf/docker_images/issues/21
# https://cuneyt.aliustaoglu.biz/en/running-gui-applications-in-docker-on-windows-linux-mac-hosts/
