#! /bin/sh

cd /fut_src/src_log
# https://github.com/rcsoccersim/rcsslogplayer/releases
tar -xzf rcsslogplayer-15.2.1.tar.gz

rm rcsslogplayer-15.2.1.tar.gz
mv rcsslogplayer-15.2.1 ../log
cd ../log


# A explicação das flags vem da seguinte Issue:
# https://github.com/rcsoccersim/rcssserver/issues/1
./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/


make
make install