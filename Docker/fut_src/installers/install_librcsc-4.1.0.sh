#! /bin/sh

cd /fut_src/src_librcs
tar -xzf librcsc-4.1.0.tar.gz
rm librcsc-4.1.0.tar.gz
mv librcsc-4.1.0 ../librcsc
cd ../librcsc

# A explicação das flags vem da seguinte Issue:
# https://github.com/rcsoccersim/rcssserver/issues/1
# ./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/
./configure

make
make install
