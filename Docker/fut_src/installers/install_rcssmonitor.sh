#! /bin/sh

cd /fut_src/src_monitor
tar -xzf rcssmonitor-15.1.1.tar.gz
rm rcssmonitor-15.1.1.tar.gz
mv rcssmonitor-15.1.1 ../monitor
cd ../monitor


# A explicação das flags vem da seguinte Issue:
# https://github.com/rcsoccersim/rcssserver/issues/1
./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/


# https://github.com/herodrigues/robocup2d-tutorial/issues/1
rm configure
cp ../src_monitor/configure ./configure


make
make install