#!/bin/bash
tar -xzf agent2d-3.1.1.tar.gz
cd agent2d-3.1.1

./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/
make
