#!/bin/bash
tar -xzf g2d-2.6.tar.gz
cd G2d-2.6

./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/
make
