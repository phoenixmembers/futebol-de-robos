#!/bin/bash
tar -xzf trilearn_base_sources-3.4.tar.gz
cd trilearn_base_sources-3.4

./configure CXXFLAGS='-std=c++03' --with-boost-libdir=/usr/lib/x86_64-linux-gnu/
make

# https://stackoverflow.com/questions/17626619/not-declared-in-this-scope-when-using-strlen/17626627
# TODO
# Adicionar a linha abaixo nos seguintes arquivos ActHandler.cpp e WorldModelUpdate e Logger e PLayer
#include <string.h>
